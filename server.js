const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const path = require('path');
const cors = require('cors');
const { readdir, unlink } = require('fs');

app.use('/form', express.static(path.join(__dirname, '/index.html')));
app.use('/css', express.static(path.join(__dirname, '/css')));
app.use('/js', express.static(path.join(__dirname, '/src')));
app.use('/assets', express.static(path.join(__dirname, '/assets')));
app.use('/files', express.static(path.join(__dirname, '/uploads')));

// default options
app.use(cors());
app.use(fileUpload());

app.post('/ping', function(req, res) {
  res.send('pong');
});

app.get('/list', function(req, res) {
  readdir(path.join(__dirname, '/uploads/'), null, (_, array) => res.send(array));
});

app.delete('/delete/:fileName', function(req, res) {
  const { fileName } = req.params;
  unlink(path.join(__dirname, `/uploads/${fileName}`), err => {
    if (err) {
      res.send(err);
      return;
    }

    res.send('File was deleted');
  });
});

app.post('/upload', function(req, res) {
  let sampleFile = null;
  let uploadPath = null;

  if (Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  sampleFile = req.files.sampleFile; // eslint-disable-line

  uploadPath = path.join(__dirname, '/uploads/', sampleFile.name);

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send(path.join('File uploaded to ', uploadPath));
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});