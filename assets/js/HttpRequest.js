define('HttpRequest', [], function() {
  class HttpRequest {
    constructor({ baseUrl = '', headers }) {
      this.baseUrl = baseUrl;
      this.headers = headers;
    }

    #urlFormatter = (url = '', params) => {
      let requestUrl = this.baseUrl + url;

      if (params) {
        requestUrl += `${requestUrl.includes('?') ? '&' : '?'}${new URLSearchParams(params)}`;
      }

      return requestUrl;
    }

    #sendRequest = config => {
      const {
        responseType = 'json',
        params,
        headers,
        data = null,
        method = 'GET',
        url,
        transformResponse,
        onDownloadProgress,
        onUploadProgress
      } = config;

      const xhr = new XMLHttpRequest();
      const formedUrl = this.#urlFormatter(url, params);
      const formedHeaders = { ...this.headers, ...headers };

      xhr.open(method, formedUrl);
      xhr.onprogress = onDownloadProgress;
      xhr.upload.onprogress = onUploadProgress;
      xhr.responseType = responseType;

      for (const header in formedHeaders) {
        xhr.setRequestHeader(header, formedHeaders[header]);
      }

      return new Promise((res, rej) => {
        xhr.onerror = rej;
        xhr.onload = () => {
          const responseObject = {
            status: xhr.status,
            responseType: xhr.responseType,
            data: xhr.response
          };

          if (responseObject.status >= 400) {
            rej(responseObject);
            return;
          }

          if (transformResponse && transformResponse.length) {
            responseObject.data = transformResponse.reduce((acc, transformFn) => transformFn(acc), responseObject.data);
          }

          res(responseObject);
        };

        xhr.send(data);
      });
    }

    get(url, config) {
      return this.#sendRequest({ ...config, method: 'GET', url });
    }

    post(url, data, config) {
      return this.#sendRequest({ ...config, method: 'POST', url, data });
    }

    delete(url, config) {
      return this.#sendRequest({ ...config, method: 'DELETE', url });
    }

    request(config) {
      return this.#sendRequest(config);
    }
  }

  return HttpRequest;
});
