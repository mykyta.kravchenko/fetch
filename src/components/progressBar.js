define('ProgressBar', [], function() {
  class ProgressBar {
    #progressNode;
    #progressContainerNode;
    #config;

    constructor(nodeContainer, options) {
      const {
        className = 'progress',
        autoremove = true,
        delayForRemove = 1000,
        withPercentage = true
      } = options;

      this.#render();
      this.#progressContainerNode.classList.add(className);
      this.#config = { autoremove, delayForRemove, withPercentage };

      nodeContainer.appendChild(this.#progressContainerNode);
    }

    #render = () => {
      this.#progressContainerNode = document.createElement('div');
      this.#progressContainerNode.setAttribute('class', 'progress');
      this.#progressNode = document.createElement('div');
      this.#progressNode.setAttribute('class', 'progress-bar');
      this.#progressContainerNode.appendChild(this.#progressNode);
    }

    #changeProgressStatus = curProgress => {
      this.#progressContainerNode.style.display = curProgress ? 'flex' : 'none';

      this.#progressNode['aria-valuenow'] = curProgress;
      this.#progressNode.style.width = `${curProgress}%`;
      this.#progressNode.innerHTML = this.#config.withPercentage ? `${curProgress}%` : '';
    }

    onProgress = ({ loaded, total }) => {
      const { autoremove, delayForRemove } = this.#config;
      const curProgress = Math.floor(loaded / total * 100);

      this.#changeProgressStatus(curProgress);

      if (curProgress === 100 && autoremove) {
        setTimeout(() => {
          this.#changeProgressStatus(0);
        }, delayForRemove);
      }
    }
  }

  return ProgressBar;
});
