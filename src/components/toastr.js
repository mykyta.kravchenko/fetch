define('Toastr', [], function() {
  class Toastr {
    #nodeContainer;
    #config = {};
    constructor(nodeContainer, { autoremove = true, delayForRemove = 3000 }) {
      this.#nodeContainer = nodeContainer;
      this.#config.autoremove = autoremove;
      this.#config.delayForRemove = delayForRemove;
    }

    #renderToastr = () => {
      const toastrWrap = document.createElement('div');
      toastrWrap.setAttribute('class', 'toastr-wrap');
      toastrWrap.innerHTML = `
        <div class="toastr"></div>
      `;

      return toastrWrap;
    }

    #renderCLoseButton = toastrNode => {
      const closeBtn = document.createElement('div');
      closeBtn.setAttribute('class', 'toastr__close-btn');
      closeBtn.innerHTML = '&times;';
      closeBtn.addEventListener('click', () => toastrNode.remove());

      toastrNode.appendChild(closeBtn);
    }

    pop(message, error) {
      const { autoremove, delayForRemove } = this.#config;
      const toastrNode = this.#renderToastr();
      const toastrTextNode = toastrNode.querySelector('.toastr');

      toastrTextNode.innerHTML = `<span>${message}</span>`;

      if (error) {
        toastrTextNode.classList.add('error-bg');
      }

      this.#nodeContainer.appendChild(toastrNode);

      if (!autoremove) {
        this.#renderCLoseButton(toastrNode);
        return;
      }

      setTimeout(() => toastrNode.remove(), delayForRemove);
    }
  }

  return Toastr;
});
