define('UploadForm', ['api'], function(api) {
  class UploadForm {
    #formNode;
    #progressTracker;
    #submitCallbacks = {
      onSuccess: [],
      onError: []
    };

    constructor(nodeContainer) {
      this.#render();
      this.#setSubmitListener();

      nodeContainer.appendChild(this.#formNode);
    }

    #render = () => {
      this.#formNode = document.createElement('form');
      this.#formNode.setAttribute('class', 'upload-form');
      this.#formNode.innerHTML = `
        <input class="form-control upload-form__file" type="file" name="sampleFile" />
        <input class="form-control upload-form__submit-btn submit-btn" type="submit" value="Upload!" />
      `;
    }

    #setSubmitListener = () => {
      this.#formNode.addEventListener('submit', e => {
        e.preventDefault();

        this.#uploadFileToServer();
      });
    }

    #formNodeRequestBody = () => {
      const [file] = this.#formNode.sampleFile.files;
      const body = new FormData();

      body.append('sampleFile', file);

      return body;
    }

    #uploadFileToServer = () => {
      const body = this.#formNodeRequestBody();

      api.uploadFile(body, { onUploadProgress: this.#progressTracker })
        .then(res => this.#submitCallbacks.onSuccess.forEach(cb => cb(res)))
        .catch(err => this.#submitCallbacks.onError.forEach(cb => cb(err)));
    }

    set onUploadProgress(cb) {
      this.#progressTracker = cb;
    }

    addCbForSuccessSubmit(cb) {
      this.#submitCallbacks.onSuccess.push(cb);
    }

    addCbForErrorSubmit(cb) {
      this.#submitCallbacks.onError.push(cb);
    }
  }

  return UploadForm;
});
