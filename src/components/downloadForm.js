define('DownloadForm', ['api'], function(api) {
  class DownloadForm {
    #formNode;
    #progressTracker;
    #submitCallbacks = {
      onSuccess: [],
      onError: []
    };

    constructor(nodeContainer) {
      this.#render();
      this.#setSubmitListener();

      nodeContainer.appendChild(this.#formNode);
    }

    #render = () => {
      this.#formNode = document.createElement('form');
      this.#formNode.setAttribute('class', 'download-form');
      this.#formNode.innerHTML = `
        <input class="form-control download-form__filename" type="text" name="sampleFile" class="sampleFile"/>
        <input class="form-control fownload-form__submit-btn submit-btn" type="submit" value="Download!" />
      `;
    }

    #setSubmitListener = () => {
      this.#formNode.addEventListener('submit', e => {
        e.preventDefault();

        this.#getFileFromServer();
      });
    }

    #getFileFromServer = () => {
      const fileName = this.#getInputValue();

      api.downloadFile(fileName, { onDownloadProgress: this.#progressTracker })
        .then(res => this.#submitCallbacks.onSuccess.forEach(cb => cb(res)))
        .catch(err => this.#submitCallbacks.onError.forEach(cb => cb(err)));
    }

    set onDownloadProgress(cb) {
      this.#progressTracker = cb;
    }

    setInputValue = string => {
      const fileNameInput = this.#formNode.querySelector('.download-form__filename');
      fileNameInput.value = string;
    }

    #getInputValue = () => {
      const { value } = this.#formNode.querySelector('.download-form__filename');

      return value;
    }

    addCbForSuccessSubmit(cb) {
      this.#submitCallbacks.onSuccess.push(cb);
    }

    addCbForErrorSubmit(cb) {
      this.#submitCallbacks.onError.push(cb);
    }
  }

  return DownloadForm;
});
