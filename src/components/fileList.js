define('FileList', ['api'], function(api) {
  class FileList {
    #list;
    #listNode;
    #onItemClick;
    #deleteCallbacks = {
      onSuccess: [],
      onError: []
    };

    constructor(nodeContainer) {
      this.#createListNode();

      this.update();
      this.#setItemClickListener();
      this.#setDeleteClickListener();

      nodeContainer.appendChild(this.#listNode);
    }

    #createListNode = () => {
      this.#listNode = document.createElement('ul');
      this.#listNode.setAttribute('class', 'file-list');
    }

    #render = () => {
      if (!this.#list?.length) {
        this.#listNode.innerHTML = '<span class="no-files-message">There\'s no files on the server!</span>';
        return;
      }

      this.#listNode.innerHTML = this.#list.reduce((htmlTemplate, fileName) =>
        `${htmlTemplate}
          <li class="file-list__file file" data-name="${fileName}">
            <a class="file__filename">${fileName}</a>
            <a href="javascript:void(0)" class="file__delete-btn">&times;</a>
          </li>
        `, '');
    }

    #setItemClickListener = () => {
      this.#listNode.addEventListener('click', ({ target }) => {
        if (!target.classList.contains('file__filename')) {
          return;
        }

        const { name } = target.parentElement.dataset;
        this.#onItemClick(name);
      });
    }

    #setDeleteClickListener = () => {
      this.#listNode.addEventListener('click', ({ target }) => {
        if (!target.classList.contains('file__delete-btn')) {
          return;
        }

        const { name } = target.parentElement.dataset;
        this.#onDeleteClick(name);
      });
    }

    #onDeleteClick = fileName => api.deleteFile(fileName)
      .then(res => {
        this.update();
        this.#deleteCallbacks.onSuccess.forEach(cb => cb(res));
      })
      .catch(err => this.#deleteCallbacks.onError.forEach(cb => cb(err)));

    set onItemClick(cb) {
      this.#onItemClick = cb;
    }

    #compareResponse = data => {
      if (this.#list?.length !== data?.length) {
        return false;
      }

      if (JSON.stringify(data) === JSON.stringify(this.#list)) {
        return true;
      }

      return false;
    }

    update() {
      api.getFileList()
        .then(({ data }) => {
          const isEqual = this.#compareResponse(data);

          if (isEqual) {
            return data;
          }

          this.#list = data;
          this.#render();
        })
        .catch(console.log); // eslint-disable-line no-console
    }

    addSuccessfullDeleteCallback(cb) {
      this.#deleteCallbacks.onSuccess.push(cb);
    }

    addErrorDeleteCallback(cb) {
      this.#deleteCallbacks.onError.push(cb);
    }
  }

  return FileList;
});
