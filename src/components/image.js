define('Picture', [], function() {
  class Picture {
    #picture;

    constructor(nodeContainer, fileUrl = '') {
      this.#render();
      this.src = fileUrl;

      nodeContainer.appendChild(this.#picture);
    }

    #render = () => {
      this.#picture = document.createElement('img');
      this.#picture.setAttribute('class', 'picture');
    }

    set src(fileUrl) {
      this.#picture.src = fileUrl;
    }
  }

  return Picture;
});
