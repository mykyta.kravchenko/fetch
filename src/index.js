define(['DownloadForm', 'UploadForm', 'FileList', 'Picture', 'Toastr', 'ProgressBar'], function(...args) {
  const [DownloadForm, UploadForm, FileList, Picture, Toastr, ProgressBar] = args;

  const app = document.querySelector('.app');

  app.innerHTML = `
    <div class="files-container"></div>
    <div class="toastr-container"></div>
    <div class="main-content">
      <div class="forms-container"></div>
      <div class="image-container"></div>
    </div>
  `;

  const formsWrapper = app.querySelector('.forms-container');
  const imageContainer = app.querySelector('.image-container');
  const filesContainer = app.querySelector('.files-container');
  const toastrContainer = app.querySelector('.toastr-container');

  const image = new Picture(imageContainer);
  const uploadForm = new UploadForm(formsWrapper);
  const downloadForm = new DownloadForm(formsWrapper);
  const fileList = new FileList(filesContainer);
  const progressLine = new ProgressBar(document.body, { className: 'progress-line', withPercentage: false });
  const progressBar = new ProgressBar(formsWrapper, { delayForRemove: 500 });
  const toastr = new Toastr(toastrContainer, { delayForRemove: 2000 });

  fileList.onItemClick = downloadForm.setInputValue;
  fileList.addSuccessfullDeleteCallback(() => toastr.pop('Successfully deleted file!'));
  fileList.addErrorDeleteCallback(() => toastr.pop('Failed to delete file!', { error: true }));

  uploadForm.onUploadProgress = progressBar.onProgress;
  uploadForm.addCbForErrorSubmit(() => toastr.pop('Failed to upload file!', { error: true }));
  uploadForm.addCbForSuccessSubmit(() => {
    fileList.update();
    toastr.pop('Successfully uploaded file!');
  });

  downloadForm.onDownloadProgress = progressLine.onProgress;
  downloadForm.addCbForErrorSubmit(() => toastr.pop('Failed to download file!', { error: true }));
  downloadForm.addCbForSuccessSubmit(({ data }) => {
    const fileUrl = URL.createObjectURL(data);

    if (!data.type.includes('image')) {
      window.downloadFile(fileUrl);
      return;
    }

    image.src = fileUrl;
    toastr.pop('Successfully downloaded image!');
  });
});
