window.downloadFile = function(pathToFile) {
  const downloadLink = document.createElement('a');
  document.body.appendChild(downloadLink);
  downloadLink.style = 'display: none';
  downloadLink.href = pathToFile;
  downloadLink.download = '';
  downloadLink.click();
  downloadLink.remove();
};