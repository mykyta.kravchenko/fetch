define('api', ['HttpRequest'], function(HttpRequest) {
  class Api {
    constructor() {
      this.hr = new HttpRequest({ baseUrl: 'http://localhost:8000' });
    }

    uploadFile(file, config) {
      return this.hr.post('/upload', file, config);
    }

    downloadFile(fileName, config) {
      return this.hr.get(`/files/${fileName}`, { responseType: 'blob', ...config });
    }

    getFileList() {
      return this.hr.get('/list');
    }

    deleteFile(fileName) {
      return this.hr.delete(`/delete/${fileName}`);
    }
  }

  return new Api();
});
